// FUNCTION superSort

function superSort(words) {
  let wordsSorted = [];
  let wordsOriginal = words.toLowerCase().split(" ");
  let wordsNoDigits = words
    .replace(/[0-9]/g, "")
    .split(" ")
    .map((element, index) => `${element}${index.toString()}`)
    .sort();

  for (let i = 0; i < wordsNoDigits.length; i++) {
    wordsSorted[i] = wordsOriginal[wordsNoDigits[i][wordsNoDigits[i].length - 1]];
  }
  return wordsSorted;
}

// FUNCTION compareDates

function compareDates(dateInput1, dateInput2) {
  let date = [];
  const dateInput = [dateInput1, dateInput2];
  const separator = [/\./, /\,/, /\//, /\-/];
  let actualSeparator;
  let result;

  for (let j = 0; j < dateInput.length; j++) {
    let day;
    let month;
    let year;

    for (let i = 0; i < separator.length; i++) {
      actualSeparator = separator[i];
      if (dateInput[j].match(separator[i]) !== null) {
        break;
      }
    }

    date[j] = dateInput[j].split(actualSeparator);

    for (let i = 0; i < date[j].length; i++) {
      if (date[j][i].length === 4) {
          year = date[j][i];
      }
      if (date[j][i].length === 2 && parseInt(date[j][i]) > 12) {
        day = date[j][i];
      }
      if (date[j][i].length === 2 && parseInt(date[j][i]) <= 12) {
        month = date[j][i];
      }
      if (
        date[j][i].length === 2 &&
        parseInt(date[j][i]) <= 12 &&
        day === undefined
      ) {
        day = date[j][i];
      }
    }

    date[j] = [day, month, year];
  }

  for (let i = 0; i < date[1].length; i++) {
    if (date[0][i] !== date[1][i]) {
      result = false;
      break;
    } 
    else {
      result = true;
    }
  }

  if (result) {
    console.log("The dates are identical");
  } else {
    console.log("The dates are not identical");
  }
}

// FUNCTION findUniqe

function findUniqe(array) {
  for (let i = 0; i < array.length; i++) {
    if (array[i] !== array[i + 1] && array[i] !== array[i - 1]) {
      return array[i];
    }
  }
}

// FUNCTION getDiscount

function getDiscount(number = 0, price = 0) {
  if (number === 0 || price === 0) {
    return price;
  }
  if (number >= 1 && number < 5) {
    return price;
  }
  if (number >= 5 && number < 10) {
    return (price * 0.95).toFixed(2);
  }
  if (number >= 10) {
    return (price * 0.9).toFixed(2);
  }
}

// FUNCTION getDaysForMonth

function getDaysForMonth(month = 0) {
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return 31 + ' days';
    case 4:
    case 6:
    case 9:
    case 11:
      return 30 + ' days';
    case 2:
      return 28 + ' days';
    default:
      return 'This is not a month';
  }
}

console.log(superSort("mic09ha1el 4b5en6 michele be4atr3ice"));

compareDates('07-22-2020', '2020,22,07');
compareDates('22.07.2020', '07/06/2020');

console.log(findUniqe([1, 1, 1, 1, 1, 5]));
console.log(findUniqe([22, 0, 0, 0, 0, 0]));

console.log(getDiscount(0, 5000));
console.log(getDiscount(4, 5000));
console.log(getDiscount(5, 5000));
console.log(getDiscount(6, 5000));
console.log(getDiscount(10, 5000));

console.log(getDaysForMonth(1));
console.log(getDaysForMonth(2));
console.log(getDaysForMonth(4));
console.log(getDaysForMonth());






