
class Fighter {
    constructor(name = 'Ryu', health = 1000, attack = 100, defense = 50) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense; 
    } 
  
    getHitPower() {
        return (this.attack*(Math.random()+1)).toFixed(0);
    }
  
    getBlockPower() {
        return (this.defense*(Math.random()+1)).toFixed(0);
    } 
  } 

export const fighter = new Fighter("Chun-Li");